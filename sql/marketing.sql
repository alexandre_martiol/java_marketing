-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 27-01-2015 a las 17:58:13
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `marketing`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `benefit`
--

CREATE TABLE IF NOT EXISTS `benefit` (
`id` int(3) NOT NULL,
  `idClient` int(3) NOT NULL,
  `idProvider` int(3) NOT NULL,
  `benefit` int(10) DEFAULT NULL,
  `paid` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client`
--

CREATE TABLE IF NOT EXISTS `client` (
`id` int(3) NOT NULL,
  `name` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `dni` varchar(9) NOT NULL,
  `location` varchar(20) NOT NULL,
  `postalCode` varchar(5) NOT NULL,
  `telephone` varchar(9) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `client`
--

INSERT INTO `client` (`id`, `name`, `lastname`, `dni`, `location`, `postalCode`, `telephone`) VALUES
(1, 'Albert', 'Culo', '39494867', 'Barcelona', '08054', '678675674'),
(2, 'Francesc', 'Sanahuja', '45792134', 'Barcelona', '08023', '659236743'),
(3, 'Alex', 'Martinez', '43568967', 'Mataró', '08045', '600764397');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provider`
--

CREATE TABLE IF NOT EXISTS `provider` (
`id` int(3) NOT NULL,
  `name` varchar(20) NOT NULL,
  `attendant` varchar(20) NOT NULL,
  `location` varchar(20) NOT NULL,
  `postalCode` varchar(5) NOT NULL,
  `dni` varchar(9) NOT NULL,
  `telephone` varchar(9) NOT NULL,
  `commission` int(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `provider`
--

INSERT INTO `provider` (`id`, `name`, `attendant`, `location`, `postalCode`, `dni`, `telephone`, `commission`) VALUES
(1, 'Muebles Brunilda', 'Bruno Keller', 'Barcelona', '08027', '43589235', '660696969', 5),
(2, 'Wood Industry S.L', 'Jaime Alcatraz', 'Olot', '05063', '46578982', '667235116', 7);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `benefit`
--
ALTER TABLE `benefit`
 ADD PRIMARY KEY (`id`), ADD KEY `idClient` (`idClient`), ADD KEY `idProvider` (`idProvider`);

--
-- Indices de la tabla `client`
--
ALTER TABLE `client`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `dni` (`dni`);

--
-- Indices de la tabla `provider`
--
ALTER TABLE `provider`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`,`dni`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `benefit`
--
ALTER TABLE `benefit`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `client`
--
ALTER TABLE `client`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `provider`
--
ALTER TABLE `provider`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `benefit`
--
ALTER TABLE `benefit`
ADD CONSTRAINT `benefit_ibfk_1` FOREIGN KEY (`idClient`) REFERENCES `client` (`id`),
ADD CONSTRAINT `benefit_ibfk_2` FOREIGN KEY (`idProvider`) REFERENCES `provider` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
