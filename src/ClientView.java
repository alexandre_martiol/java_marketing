import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * Created by AleM on 13/02/15.
 */
public class ClientView extends JFrame {
    private static DBConnection con = new DBConnection();
    private static JPanel contentPanel;

    private static JMenuBar menubar;

    private JTable clientsTable;
    private DefaultTableModel model;

    private JButton btnModifyClient;
    private JButton btnAddClient;

    private ArrayList<Client> clientsArray;

    private TableRowSorter<TableModel> rowSorter;

    private JTextField textFieldFilter;

    private JLabel searchLabel;
    //*************************************** END DEFINITION OF VARIABLES ******************************************

    private void resetClientsTable() {
        model.setRowCount(0);
    }

    private void addInfoInTable() {
        //instruccio SQL PER BUSCAR TOTS ELS REGISTRES DE LA TAULA BENEFIT
        clientsArray = con.getClients();

        for (int i = 0; i < clientsArray.size(); i++) {
            //Creamos un Objeto con tantos parámetros como datos retorne cada fila de la consulta
            Object[] fila = new Object[6];

            //Lo que hay entre comillas son los campos de la base de datos
            //fila[0] = clientsArray.get(i).getId();
            fila[0] = clientsArray.get(i).getName();
            fila[1] = clientsArray.get(i).getLastname();
            fila[2] = clientsArray.get(i).getDni();
            fila[3] = clientsArray.get(i).getLocation();
            fila[4] = clientsArray.get(i).getPostalCode();
            fila[5] = clientsArray.get(i).getTelephone();

            model.addRow(fila); // Añade una fila al final del model de la tabla
        }
    }

    protected void createMenu() {
        menubar = new JMenuBar();

        //***************************************MAIN MENU******************************************
        JMenu mainMenu = new JMenu("Main Menu");
        mainMenu.setMnemonic(KeyEvent.VK_F);
        mainMenu.setToolTipText("Go to Main Menu");
        mainMenu.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new Main().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(mainMenu);

        //***************************************ADD MENU******************************************
        JMenu addMenu = new JMenu("Add Transaction");
        addMenu.setMnemonic(KeyEvent.VK_F);
        addMenu.setToolTipText("Add new transaction");
        addMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new AddTransaction().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(addMenu);

        //***************************************PROVIDER MENU******************************************
        JMenu providerMenu = new JMenu("Provider");
        providerMenu.setMnemonic(KeyEvent.VK_F);
        providerMenu.setToolTipText("Provider");
        providerMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new ProviderView().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(providerMenu);

        //***************************************EXIT MENU******************************************
        JMenu exitMenu = new JMenu("Exit");
        exitMenu.setMnemonic(KeyEvent.VK_F);
        exitMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(exitMenu);

        //***************************************SETTING UP MENU******************************************
        setJMenuBar(menubar);

        setTitle("Client Management");
        setSize(500, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public ClientView() {
        // boto nova venta configuracio , preparacio event I crida a seguent pantalla de la classe AltesFrame
        setBounds(500, 200, 500, 600);
        contentPanel = new JPanel();
        SpringLayout layout = new SpringLayout();
        contentPanel.setLayout(layout);

        //***************************************WEST REGION******************************************
        JScrollPane scrollPanel = new JScrollPane();
        scrollPanel.setBounds(20, 50, 300, 300);

        layout.putConstraint(SpringLayout.WEST, scrollPanel, 25, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, scrollPanel, 50, SpringLayout.WEST, contentPanel);

        contentPanel.add(scrollPanel);

        try {
            //PREPARACIO DE LA TAULA , Afegim columnes a la taula (les que volguem)
            model = new DefaultTableModel() {
                @Override
                public boolean isCellEditable(int row, int column) {
                    //all cells false
                    return false;
                }
            };

            //model.addColumn("Num");
            model.addColumn("Name");
            model.addColumn("LastName");
            model.addColumn("DNI");
            model.addColumn("Location");
            model.addColumn("PostalCode");
            model.addColumn("Telephone");

            addInfoInTable();

            //Esto añade la tabla al portView del scrollPanel
            clientsTable = new JTable(model);
            TableColumnModel columnModel = clientsTable.getColumnModel();
            columnModel.getColumn(0).setPreferredWidth(20);
            columnModel.getColumn(1).setPreferredWidth(20);
            columnModel.getColumn(2).setPreferredWidth(20);
            columnModel.getColumn(3).setPreferredWidth(20);
            columnModel.getColumn(4).setPreferredWidth(20);
            columnModel.getColumn(5).setPreferredWidth(30);
            //columnModel.getColumn(6).setPreferredWidth(20);

            scrollPanel.setViewportView(clientsTable);

        }catch (Exception e){
            e.printStackTrace();
        }

        //***************************************SOUTH REGION******************************************
        btnModifyClient = new JButton("Modify Client");
        btnModifyClient.setBounds(1480, 1180, 118, 23);
        btnModifyClient.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                int selectedRow = clientsTable.getSelectedRow();

                //If not selected row...
                if (selectedRow == -1) {
                    JOptionPane.showMessageDialog(null, "Please first select a client.");
                }

                else {
                    int selectedClient  = clientsArray.get(selectedRow).getId();
                    Client c = con.getClient(selectedClient);

                    dispose();
                    new ModifyClient(c).setVisible(true);
                }
            }
        });

        layout.putConstraint(SpringLayout.WEST, btnModifyClient, 60, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, btnModifyClient, 500, SpringLayout.WEST, contentPanel);
        contentPanel.add(btnModifyClient);

        btnAddClient = new JButton("Add new Client");
        btnAddClient.setBounds(1480, 1180, 118, 23);
        btnAddClient.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                dispose();
                new AddClient().setVisible(true);

            }
        });

        layout.putConstraint(SpringLayout.WEST, btnAddClient, 300, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, btnAddClient, 500, SpringLayout.WEST, contentPanel);
        contentPanel.add(btnAddClient);

        //***************************************NORTH REGION******************************************
        this.createMenu();

        searchLabel = new JLabel("Search:");
        layout.putConstraint(SpringLayout.WEST, searchLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, searchLabel, 20, SpringLayout.WEST, contentPanel);

        contentPanel.add(searchLabel);

        rowSorter = new TableRowSorter<TableModel>(clientsTable.getModel());
        textFieldFilter = new JTextField();
        textFieldFilter.setPreferredSize(new Dimension(150, 30));

        clientsTable.setRowSorter(rowSorter);

        layout.putConstraint(SpringLayout.WEST, textFieldFilter, 70, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, textFieldFilter, 15, SpringLayout.WEST, contentPanel);
        contentPanel.add(textFieldFilter);

        textFieldFilter.getDocument().addDocumentListener(new DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                String text = textFieldFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                String text = textFieldFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

        });

        //*************************************** CONFIGURATION ******************************************
        searchLabel.setVisible(true);
        textFieldFilter.setVisible(true);
        btnModifyClient.setVisible(true);
        btnAddClient.setVisible(true);
        contentPanel.setVisible(true);
        this.add(contentPanel);
    }
}
