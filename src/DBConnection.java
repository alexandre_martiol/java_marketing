/**
 * Created by AleM on 02/02/15.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class DBConnection {
    public Connection connection;
    private Statement s;

    public DBConnection() {
        try {
            //Registrar driver MySQL
            DriverManager.registerDriver(new org.gjt.mm.mysql.Driver());

            //Connexio a la BBDD
            connection = DriverManager.getConnection("jdbc:mysql://localhost/marketing", "root", null);

            //Crear Statement per a realitzar consultes
            s = connection.createStatement();
        }

        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public Benefit getTransaction(int i) {
        Benefit b = new Benefit();

        try {
            // Se realiza la consulta. Los resultados se guardan en el ResultSet rs
            ResultSet rs = s.executeQuery("select * from benefit where id = " + i + "");

            // Se recorre el ResultSet, mostrando por pantalla los resultados.
            while (rs.next()) {
                int id = (Integer) rs.getObject("id");
                int idClient = (Integer) rs.getObject("idClient");
                int idProvider = (Integer) rs.getObject("idProvider");
                float benefit = (Float) rs.getObject("benefit");
                boolean paid = ((Boolean) rs.getObject("paid")).booleanValue();

                b = new Benefit(id, idClient, idProvider, benefit, paid);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return b;
    }

    public ArrayList<Benefit> getUserTransactions(int i) {
        ArrayList<Benefit> userTransactions = new ArrayList<Benefit>();
        Benefit b = new Benefit();

        try {
            // Se realiza la consulta. Los resultados se guardan en el ResultSet rs
            ResultSet rs = s.executeQuery("select * from benefit where idClient = " + i + "");

            // Se recorre el ResultSet, mostrando por pantalla los resultados.
            while (rs.next()) {
                int id = (Integer) rs.getObject("id");
                int idClient = (Integer) rs.getObject("idClient");
                int idProvider = (Integer) rs.getObject("idProvider");
                float benefit = (Float) rs.getObject("benefit");
                boolean paid = ((Boolean) rs.getObject("paid")).booleanValue();

                b = new Benefit(id, idClient, idProvider, benefit, paid);
                userTransactions.add(b);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return userTransactions;
    }

    public ArrayList<Benefit> getProviderTransactions(int i) {
        ArrayList<Benefit> providerTransactions = new ArrayList<Benefit>();
        Benefit b = new Benefit();

        try {
            // Se realiza la consulta. Los resultados se guardan en el ResultSet rs
            ResultSet rs = s.executeQuery("select * from benefit where idProvider = " + i + "");

            // Se recorre el ResultSet, mostrando por pantalla los resultados.
            while (rs.next()) {
                int id = (Integer) rs.getObject("id");
                int idClient = (Integer) rs.getObject("idClient");
                int idProvider = (Integer) rs.getObject("idProvider");
                float benefit = (Float) rs.getObject("benefit");
                boolean paid = ((Boolean) rs.getObject("paid")).booleanValue();

                b = new Benefit(id, idClient, idProvider, benefit, paid);
                providerTransactions.add(b);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return providerTransactions;
    }

    public ArrayList<Benefit> getTransactions() {
        Benefit b;
        ArrayList<Benefit> array = new ArrayList<Benefit>();

        try {
            // Se realiza la consulta. Los resultados se guardan en el ResultSet rs
            ResultSet rs = s.executeQuery("select * from benefit");

            // Se recorre el ResultSet, mostrando por pantalla los resultados.
            while (rs.next()) {
                int id = (Integer) rs.getObject("id");
                int idClient = (Integer) rs.getObject("idClient");
                int idProvider = (Integer) rs.getObject("idProvider");
                float benefit = (Float) rs.getObject("benefit");
                boolean paid = ((Boolean) rs.getObject("paid")).booleanValue();

                b = new Benefit(id, idClient, idProvider, benefit, paid);

                array.add(b);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return array;
    }

    public ArrayList<Benefit> getPaidTransactions() {
        Benefit b;
        ArrayList<Benefit> array = new ArrayList<Benefit>();

        try {
            // Se realiza la consulta. Los resultados se guardan en el ResultSet rs
            ResultSet rs = s.executeQuery("select * from benefit where paid = true");

            // Se recorre el ResultSet, mostrando por pantalla los resultados.
            while (rs.next()) {
                int id = (Integer) rs.getObject("id");
                int idClient = (Integer) rs.getObject("idClient");
                int idProvider = (Integer) rs.getObject("idProvider");
                float benefit = (Float) rs.getObject("benefit");
                boolean paid = ((Boolean) rs.getObject("paid")).booleanValue();

                b = new Benefit(id, idClient, idProvider, benefit, paid);

                array.add(b);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return array;
    }

    public ArrayList<Benefit> getNonPaidTransactions() {
        Benefit b;
        ArrayList<Benefit> array = new ArrayList<Benefit>();

        try {
            // Se realiza la consulta. Los resultados se guardan en el ResultSet rs
            ResultSet rs = s.executeQuery("select * from benefit where paid = false");

            // Se recorre el ResultSet, mostrando por pantalla los resultados.
            while (rs.next()) {
                int id = (Integer) rs.getObject("id");
                int idClient = (Integer) rs.getObject("idClient");
                int idProvider = (Integer) rs.getObject("idProvider");
                float benefit = (Float) rs.getObject("benefit");
                boolean paid = ((Boolean) rs.getObject("paid")).booleanValue();

                b = new Benefit(id, idClient, idProvider, benefit, paid);

                array.add(b);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return array;
    }

    public void addTransaction(int idC, int idP, float b, int p) {
        try {
            String query = "INSERT INTO benefit VALUES (0, " + idC+ ", " + idP + ", " + b + ", " + p + ")";

            System.out.println("Query: " + query);
            int rs = s.executeUpdate(query);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateTransaction(int id, int idC, int idP, float b, int p) {
        try {
            String query = "UPDATE benefit SET idClient = " + idC + ", idProvider = " + idP + ", benefit = " + b + ", paid = " + p + " WHERE id = " + id;

            System.out.println("Query: " + query);
            int rs = s.executeUpdate(query);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteTransaction(int id) {
        try {
            String query = "DELETE FROM BENEFIT WHERE id = " + id;

            System.out.println("Query: " + query);
            int rs = s.executeUpdate(query);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void payTransactions(int[] selectedTransactions) {
        try {
            for (int i  = 0; i < selectedTransactions.length; i++) {
                String query = "UPDATE benefit SET paid = 1 WHERE id = " + selectedTransactions[i] + "";

                System.out.println("Query: " + query);
                int rs = s.executeUpdate(query);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Client getClient(int i) {
        Client c = new Client();

        try {
            // Se realiza la consulta. Los resultados se guardan en el ResultSet rs
            ResultSet rs = s.executeQuery("select * from client where id = " + i + "");

            // Se recorre el ResultSet, mostrando por pantalla los resultados.
            while (rs.next()) {
                int id = (Integer) rs.getObject("id");
                String name = (String) rs.getObject("name");
                String lastname = (String) rs.getObject("lastname");
                String dni = (String) rs.getObject("dni");
                String location = (String) rs.getObject("location");
                String postalCode = (String) rs.getObject("postalCode");
                String telephone = (String) rs.getObject("telephone");

                c = new Client(id, name, lastname, dni, location, postalCode, telephone);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return c;
    }

    public ArrayList<Client> getClients() {
        ArrayList<Client> array = new ArrayList<Client>();

        try {
            // Se realiza la consulta. Los resultados se guardan en el ResultSet rs
            ResultSet rs = s.executeQuery("select * from client");

            // Se recorre el ResultSet, mostrando por pantalla los resultados.
            while (rs.next()) {
                int id = (Integer) rs.getObject("id");
                String name = (String) rs.getObject("name");
                String lastname = (String) rs.getObject("lastname");
                String dni = (String) rs.getObject("dni");
                String location = (String) rs.getObject("location");
                String postalCode = (String) rs.getObject("postalCode");
                String telephone = (String) rs.getObject("telephone");

                array.add(new Client(id, name, lastname, dni, location, postalCode, telephone));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return array;
    }

    public void addClient(String n, String l, String d, String lo, String p, String t) {
        try {
            String query = "INSERT INTO client VALUES (0, '" + n + "', '" + l + "', '" + d + "', '" + lo + "', '"  + p + "','"  + t + "')";

            System.out.println("Query: " + query);
            int rs = s.executeUpdate(query);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void modifyClient(int i, String n, String l, String d, String lo, String p, String t) {
        try {
            String query = "UPDATE client set name = '" + n + "', lastname = '" + l + "', location = '" + lo + "', postalCode = '"  + p + "', telephone = '"  + t + "' where id = '" + i + "'";

            System.out.println("Query: " + query);
            int rs = s.executeUpdate(query);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteClient(int id) {
        try {
            String query = "DELETE FROM CLIENT WHERE id = " + id;

            System.out.println("Query: " + query);
            int rs = s.executeUpdate(query);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Provider getProvider(int i) {
        Provider provider = new Provider();

        try {
            // Se realiza la consulta. Los resultados se guardan en el ResultSet rs
            ResultSet rs = s.executeQuery("select * from provider where id = " + i + "");

            // Se recorre el ResultSet, mostrando por pantalla los resultados.
            while (rs.next()) {
                int id = (Integer) rs.getObject("id");
                String name = (String) rs.getObject("name");
                String attendant = (String) rs.getObject("attendant");
                String location = (String) rs.getObject("location");
                String postalCode = (String) rs.getObject("postalCode");
                String dni = (String) rs.getObject("dni");
                String telephone = (String) rs.getObject("telephone");
                int commission = (Integer) rs.getObject("commission");

                provider = new Provider(id, name, attendant, location, postalCode, dni, telephone, commission);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return provider;
    }

    public ArrayList<Provider> getProviders() {
        Provider p;
        ArrayList<Provider> array = new ArrayList<Provider>();

        try {
            // Se realiza la consulta. Los resultados se guardan en el ResultSet rs
            ResultSet rs = s.executeQuery("select * from Provider");

            // Se recorre el ResultSet, mostrando por pantalla los resultados.
            while (rs.next()) {
                int id = (Integer) rs.getObject("id");
                String name = (String) rs.getObject("name");
                String attendant = (String) rs.getObject("attendant");
                String location = (String) rs.getObject("location");
                String postalCode = (String) rs.getObject("postalCode");
                String dni = (String) rs.getObject("dni");
                String telephone = (String) rs.getObject("telephone");
                int commission = (Integer) rs.getObject("commission");

                p = new Provider(id, name, attendant, location, postalCode, telephone, dni, commission);
                array.add(p);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return array;
    }

    public void addProvider(String n, String a, String d, String lo, String p, String t, String c) {
        try {
            String query = "INSERT INTO provider VALUES (0, '" + n + "', '" + a + "', '" + lo + "', '" + p + "', '"  + d + "', '"  + t + "','"  + c + "')";

            System.out.println("Query: " + query);
            int rs = s.executeUpdate(query);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void modifyProvider(int i, String n, String a, String d, String lo, String p, String t, String c) {
        try {
            String query = "UPDATE provider set name = '" + n + "', attendant = '" + a + "', location = '" + lo + "', postalCode = '" + p + "', telephone = '"  + t + "', commission = '"  + c + "' where id = '" + i + "'";

            System.out.println("Query: " + query);
            int rs = s.executeUpdate(query);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteProvider(int id) {
        try {
            String query = "DELETE FROM PROVIDER WHERE id = " + id;

            System.out.println("Query: " + query);
            int rs = s.executeUpdate(query);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

