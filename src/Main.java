/**
 * Created by AleM on 02/02/15.
 */

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.ArrayList;

public class Main extends JFrame {
    private static DBConnection con = new DBConnection();
    private static JPanel contentPanel;

    private static JMenuBar menubar;

    private JTable transactionsTable;
    private DefaultTableModel model;

    private JComboBox showTransactionsChoise;
    private JButton btnModifyTransaction;
    private JButton btnPayTransaction;

    private JLabel totalMoney = new JLabel();
    private JLabel paidMoney = new JLabel();
    private JLabel nonPaidMoney = new JLabel();

    private JRadioButton radioButton = new JRadioButton();

    private ArrayList<Benefit> transactionsArray;
    //*************************************** END DEFINITION OF VARIABLES ******************************************

    private void resetTransactionsTable() {
        model.setRowCount(0);
    }

    private void addInfoInTable(int option) {
        //instruccio SQL PER BUSCAR TOTS ELS REGISTRES DE LA TAULA BENEFIT
        ArrayList<Client> clientsArray = con.getClients();
        ArrayList<Provider> providersArray = con.getProviders();

        if (option == 0) {
            transactionsArray = con.getTransactions();

            int money = 0;
            for (int i = 0; i < transactionsArray.size(); i++) {
                money += transactionsArray.get(i).getBenefit();
            }

            totalMoney.setText("Total Money: " + money + " euros");

            totalMoney.setVisible(true);
            paidMoney.setVisible(false);
            nonPaidMoney.setVisible(false);
        } else if (option == 1) {
            transactionsArray = con.getPaidTransactions();

            int money = 0;
            for (int i = 0; i < transactionsArray.size(); i++) {
                money += transactionsArray.get(i).getBenefit();
            }

            paidMoney.setText("Paid Money: " + money + " euros");

            totalMoney.setVisible(false);
            paidMoney.setVisible(true);
            nonPaidMoney.setVisible(false);
        } else {
            transactionsArray = con.getNonPaidTransactions();

            int money = 0;
            for (int i = 0; i < transactionsArray.size(); i++) {
                money += transactionsArray.get(i).getBenefit();
            }

            nonPaidMoney.setText("Non Paid Money: " + money + " euros");

            totalMoney.setVisible(false);
            paidMoney.setVisible(false);
            nonPaidMoney.setVisible(true);
        }

        for (int i = 0; i < transactionsArray.size(); i++) {
            //Creamos un Objeto con tantos parámetros como datos retorne cada fila de la consulta
            Object[] fila = new Object[5];

            //Lo que hay entre comillas son los campos de la base de datos
            fila[0] = transactionsArray.get(i).getId();
            fila[1] = (clientsArray.get((Integer) transactionsArray.get(i).getIdClient() - 1).getName() + " " + clientsArray.get((Integer) transactionsArray.get(i).getIdClient() - 1).getLastname());
            fila[2] = (providersArray.get((Integer) transactionsArray.get(i).getIdProvider() - 1).getName());
            fila[3] = transactionsArray.get(i).getBenefit();

            if (transactionsArray.get(i).isPaid() == true) {
                fila[4] = "YES";
            } else {
                fila[4] = "NO";
            }

            model.addRow(fila); // Añade una fila al final del model de la tabla
        }
    }

    protected void createMenu() {
        menubar = new JMenuBar();

        //***************************************ADD MENU******************************************
        JMenu addMenu = new JMenu("Add Transaction");
        addMenu.setMnemonic(KeyEvent.VK_F);
        addMenu.setToolTipText("Add new transaction");
        addMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new AddTransaction().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(addMenu);

        //***************************************CLIENT MENU******************************************
        JMenu clientMenu = new JMenu("Client");
        clientMenu.setMnemonic(KeyEvent.VK_F);
        clientMenu.setToolTipText("Client");
        clientMenu.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new ClientView().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(clientMenu);

        //***************************************PROVIDER MENU******************************************
        JMenu providerMenu = new JMenu("Provider");
        providerMenu.setMnemonic(KeyEvent.VK_F);
        providerMenu.setToolTipText("Provider");
        providerMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new ProviderView().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(providerMenu);

        //***************************************EXIT MENU******************************************
        JMenu exitMenu = new JMenu("Exit");
        exitMenu.setMnemonic(KeyEvent.VK_F);
        exitMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(exitMenu);

        //***************************************SETTING UP MENU******************************************
        setJMenuBar(menubar);

        setTitle("Marketing Management");
        setSize(500, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public Main() {
        // boto nova venta configuracio , preparacio event I crida a seguent pantalla de la classe AltesFrame
        setBounds(500, 200, 500, 600);
        contentPanel = new JPanel();
        SpringLayout layout = new SpringLayout();
        contentPanel.setLayout(layout);

        //***************************************NORTH REGION******************************************
        this.createMenu();

        showTransactionsChoise = new JComboBox();
        showTransactionsChoise.addItem("All Transactions");
        showTransactionsChoise.addItem("Paid Transactions");
        showTransactionsChoise.addItem("Non Paid Transactions");

        showTransactionsChoise.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetTransactionsTable();
                addInfoInTable(showTransactionsChoise.getSelectedIndex());
            }
        });

        layout.putConstraint(SpringLayout.WEST, showTransactionsChoise, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, showTransactionsChoise, 15, SpringLayout.WEST, contentPanel);
        contentPanel.add(showTransactionsChoise);

        //totalMoney.setText("Total Money: 0");
        //paidMoney.setText("Paid Money: 0");
        //nonPaidMoney.setText("Non Paid Money: 0");

        paidMoney.setForeground(Color.green);
        nonPaidMoney.setForeground(Color.red);

        layout.putConstraint(SpringLayout.WEST, totalMoney, 300, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, totalMoney, 20, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, paidMoney, 300, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, paidMoney, 20, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, nonPaidMoney, 300, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, nonPaidMoney, 20, SpringLayout.WEST, contentPanel);

        contentPanel.add(totalMoney);
        contentPanel.add(paidMoney);
        contentPanel.add(nonPaidMoney);

        //***************************************WEST REGION******************************************
        JScrollPane scrollPanel = new JScrollPane();
        scrollPanel.setBounds(20, 50, 300, 300);

        layout.putConstraint(SpringLayout.WEST, scrollPanel, 25, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, scrollPanel, 50, SpringLayout.WEST, contentPanel);

        contentPanel.add(scrollPanel);

        try {
            //PREPARACIO DE LA TAULA , Afegim columnes a la taula (les que volguem)
            model = new DefaultTableModel() {
                @Override
                public boolean isCellEditable(int row, int column) {
                    //all cells false
                    return false;
                }
            };

            model.addColumn("Num");
            model.addColumn("Client");
            model.addColumn("Provider");
            model.addColumn("Benefit");
            model.addColumn("Paid");

            addInfoInTable(0);

            //Esto añade la tabla al portView del scrollPanel
            transactionsTable = new JTable(model);
            TableColumnModel columnModel = transactionsTable.getColumnModel();
            columnModel.getColumn(0).setPreferredWidth(10);
            columnModel.getColumn(1).setPreferredWidth(80);
            columnModel.getColumn(2).setPreferredWidth(80);
            columnModel.getColumn(3).setPreferredWidth(30);
            columnModel.getColumn(4).setPreferredWidth(20);

            scrollPanel.setViewportView(transactionsTable);

        }catch (Exception e){
            e.printStackTrace();
        }

        //***************************************SOUTH REGION******************************************
        btnModifyTransaction = new JButton("Modify Transaction");
        btnModifyTransaction.setBounds(1480, 1180, 118, 23);
        btnModifyTransaction.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                int selectedRow = transactionsTable.getSelectedRow();

                //If not selected row...
                if (selectedRow == -1) {
                    JOptionPane.showMessageDialog(null, "Please first select a transaction.");
                }

                else {
                    int selectedTransaction = transactionsArray.get(selectedRow).getId();

                    Benefit b = con.getTransaction(selectedTransaction);
                    dispose();
                    new ModifyTransaction(b).setVisible(true);
                }
            }
        });

        layout.putConstraint(SpringLayout.WEST, btnModifyTransaction, 60, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, btnModifyTransaction, 500, SpringLayout.WEST, contentPanel);
        contentPanel.add(btnModifyTransaction);

        btnPayTransaction = new JButton("Earn Transaction");
        btnPayTransaction.setBounds(1480, 1180, 118, 23);
        btnPayTransaction.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                int seleccion = JOptionPane.showOptionDialog(
                        contentPanel,
                        "Do you want to earn these transactions?",
                        "Earn Transactions",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,    // null para icono por defecto.
                        new Object[]{"Earn", "Cancel"},   // null para YES, NO y CANCEL
                        "Earn");

                if (seleccion == 0) {
                    int[] selectedRows = transactionsTable.getSelectedRows();

                    int[] selectedTransactions = new int[selectedRows.length];
                    for (int i = 0; i < selectedRows.length; i++) {
                        selectedTransactions[i] = transactionsArray.get(selectedRows[i]).getId();
                    }

                    con.payTransactions(selectedTransactions);

                    resetTransactionsTable();
                    addInfoInTable(0);
                }
            }
        });

        layout.putConstraint(SpringLayout.WEST, btnPayTransaction, 300, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, btnPayTransaction, 500, SpringLayout.WEST, contentPanel);
        contentPanel.add(btnPayTransaction);

        //*************************************** CONFIGURATION ******************************************
        btnModifyTransaction.setVisible(true);
        btnPayTransaction.setVisible(true);
        showTransactionsChoise.setVisible(true);
        contentPanel.setVisible(true);
        this.add(contentPanel);
    }

    public static void main(String[] args) throws SQLException {
        try {
            Main frame = new Main();
            frame.setVisible(true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}