import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Created by AleM on 06/02/15.
 */
public class ModifyTransaction extends JFrame implements ItemListener {
    private static DBConnection con = new DBConnection();
    private ArrayList<Client> arrayClients = new ArrayList<Client>();
    private ArrayList<Provider> arrayProviders = new ArrayList<Provider>();
    private Benefit b;

    private static JPanel contentPanel;
    private static JMenuBar menubar;

    private static JLabel infoBox;
    private static JLabel clientLabelInfo;
    private static JLabel providerLabelInfo;
    private static JLabel importLabelInfo;
    private static JLabel benefitLabelInfo;
    private static JLabel paidLabelInfo;

    private JTextField importTextInfo;
    private JLabel benefitTextInfo;

    private static JComboBox clientChoise;
    private static JComboBox providerChoise;
    private static JComboBox paidChoise;

    private static JButton btnModifyTransaction;
    private static JButton btnDeleteTransaction;
    //*************************************** END DEFINITION OF VARIABLES ******************************************

    private void completeClientBox() {
        for (int i = 0; i < arrayClients.size(); i++) {
            clientChoise.addItem(arrayClients.get(i).getName() + " " + arrayClients.get(i).getLastname());
        }
    }

    private void completeProviderBox() {
        for (int i = 0; i < arrayProviders.size(); i++) {
            providerChoise.addItem(arrayProviders.get(i).getName());
        }
    }

    private void updateTransaction() {
        con.updateTransaction(b.getId(), clientChoise.getSelectedIndex() + 1, providerChoise.getSelectedIndex() + 1, Float.parseFloat(benefitTextInfo.getText()), paidChoise.getSelectedIndex());
    }

    private void deleteTransaction() {
        con.deleteTransaction(b.getId());
    }

    protected void createMenu() {
        menubar = new JMenuBar();

        //***************************************ADD MENU******************************************
        JMenu addMenu = new JMenu("Main Menu");
        addMenu.setMnemonic(KeyEvent.VK_F);
        addMenu.setToolTipText("Go to Main Menu");
        addMenu.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new Main().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(addMenu);

        //***************************************CLIENT MENU******************************************
        JMenu clientMenu = new JMenu("Client");
        clientMenu.setMnemonic(KeyEvent.VK_F);
        clientMenu.setToolTipText("Client");
        clientMenu.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new ClientView().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(clientMenu);

        //***************************************PROVIDER MENU******************************************
        JMenu providerMenu = new JMenu("Provider");
        providerMenu.setMnemonic(KeyEvent.VK_F);
        providerMenu.setToolTipText("Provider");
        providerMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new ProviderView().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(providerMenu);

        //***************************************EXIT MENU******************************************
        JMenu exitMenu = new JMenu("Exit");
        exitMenu.setMnemonic(KeyEvent.VK_F);
        exitMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(exitMenu);

        //***************************************SETTING UP MENU******************************************
        setJMenuBar(menubar);

        setTitle("Modify Transaction");
        setSize(500, 350);
        setLocationRelativeTo(null);
    }

    public ModifyTransaction(Benefit benefit){
        b = benefit;
        arrayClients = con.getClients();
        arrayProviders = con.getProviders();

        setBounds(500, 200, 500, 350);
        contentPanel = new JPanel();
        SpringLayout layout = new SpringLayout();
        contentPanel.setLayout(layout);
        contentPanel.setLayout(layout);

        //***************************************NORTH REGION******************************************
        infoBox = new JLabel("INFORMATION ABOUT THE TRANSACTION:");
        clientLabelInfo = new JLabel("Client:");
        providerLabelInfo = new JLabel("Provider:");
        importLabelInfo = new JLabel("Total import:");
        benefitLabelInfo = new JLabel("Benefit:");
        paidLabelInfo = new JLabel("Paid:");

        clientChoise = new JComboBox();
        providerChoise = new JComboBox();

        benefitTextInfo = new JLabel("0");
        importTextInfo = new JTextField("0");

        importTextInfo.setPreferredSize(new Dimension(70, 30));
        importTextInfo.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if (!importTextInfo.getText().isEmpty()) {
                    int i = providerChoise.getSelectedIndex();
                    int commission = arrayProviders.get(i).getCommission();
                    int totalImport = Integer.parseInt(importTextInfo.getText());
                    System.out.println(i + " " + commission + " " + totalImport);
                    float benefit = (totalImport * commission) / 100;

                    benefitTextInfo.setText(Float.toString(benefit));
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if (!importTextInfo.getText().isEmpty()) {
                    int i = providerChoise.getSelectedIndex();
                    int commission = arrayProviders.get(i).getCommission();
                    int totalImport = Integer.parseInt(importTextInfo.getText());
                    System.out.println(i + " " + commission + " " + totalImport);
                    float benefit = (totalImport * commission) / 100;

                    benefitTextInfo.setText(Float.toString(benefit));
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                if (!importTextInfo.getText().isEmpty()) {
                    int i = providerChoise.getSelectedIndex();
                    int commission = arrayProviders.get(i).getCommission();
                    int totalImport = Integer.parseInt(importTextInfo.getText());

                    float benefit = (totalImport * commission) / 100;

                    benefitTextInfo.setText(Float.toString(benefit));
                }
            }
        });

        paidChoise = new JComboBox();
        paidChoise.addItem("NO");
        paidChoise.addItem("YES");

        contentPanel.add(infoBox);
        contentPanel.add(clientLabelInfo);
        contentPanel.add(providerLabelInfo);
        contentPanel.add(importLabelInfo);
        contentPanel.add(benefitLabelInfo);
        contentPanel.add(paidLabelInfo);
        contentPanel.add(paidChoise);
        contentPanel.add(clientChoise);
        contentPanel.add(providerChoise);
        contentPanel.add(importTextInfo);
        contentPanel.add(benefitTextInfo);

        layout.putConstraint(SpringLayout.WEST, infoBox, 100, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, infoBox, 20, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, clientLabelInfo, 100, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, clientLabelInfo, 50, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, providerLabelInfo, 100, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, providerLabelInfo, 80, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, importLabelInfo, 100, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, importLabelInfo, 110, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, benefitLabelInfo, 100, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, benefitLabelInfo, 140, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, paidLabelInfo, 100, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, paidLabelInfo, 170, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, paidChoise, 140, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, paidChoise, 165, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, clientChoise, 160, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, clientChoise, 45, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, providerChoise, 160, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, providerChoise, 75, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, importTextInfo, 180, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, importTextInfo, 105, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, benefitTextInfo, 180, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, benefitTextInfo, 140, SpringLayout.WEST, contentPanel);

        //*************************************** SOUTH REGION ******************************************
        btnModifyTransaction = new JButton("Modify Transaction");
        btnModifyTransaction.setBounds(1480, 1180, 118, 23);
        btnModifyTransaction.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                int seleccion = JOptionPane.showOptionDialog(
                        contentPanel,
                        "Do you want to modify this transaction?",
                        "Modify Transaction",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,    // null para icono por defecto.
                        new Object[]{"Modify", "Cancel"},   // null para YES, NO y CANCEL
                        "Modify");

                if (seleccion == 0) {
                    updateTransaction();

                    dispose();
                    new Main().setVisible(true);
                }
            }
        });

        layout.putConstraint(SpringLayout.WEST, btnModifyTransaction, 80, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, btnModifyTransaction, 230, SpringLayout.WEST, contentPanel);

        btnDeleteTransaction = new JButton("Delete Transaction");
        btnDeleteTransaction.setBounds(1480, 1180, 118, 23);
        btnDeleteTransaction.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                int seleccion = JOptionPane.showOptionDialog(
                        contentPanel,
                        "Do you want to delete this transaction?",
                        "Delete Transaction",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,    // null para icono por defecto.
                        new Object[]{"Delete", "Cancel"},   // null para YES, NO y CANCEL
                        "Delete");

                if (seleccion == 0) {
                    deleteTransaction();

                    dispose();
                    new Main().setVisible(true);
                }
            }
        });

        layout.putConstraint(SpringLayout.WEST, btnDeleteTransaction, 240, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, btnDeleteTransaction, 230, SpringLayout.WEST, contentPanel);

        contentPanel.add(btnModifyTransaction);
        contentPanel.add(btnDeleteTransaction);

        //*************************************** CONFIGURATION ******************************************
        this.completeProviderBox();
        this.completeClientBox();
        this.createMenu();
        this.add(contentPanel);
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }
}
