/**
 * Created by AleM on 02/02/15.
 */

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class AddTransaction extends JFrame implements ItemListener{
    private static DBConnection con = new DBConnection();
    private ArrayList<Client> arrayClients = new ArrayList<Client>();
    private ArrayList<Provider> arrayProviders = new ArrayList<Provider>();

    private static JPanel contentPanel;
    private static JMenuBar menubar;

    private static JButton btnAddTransaction;
    private static JButton btnCancelTransaction;

    private static JLabel clientLabel;
    private static JLabel providerLabel;
    private static JLabel importLabel;

    private static JLabel infoBox;
    private static JLabel clientLabelInfo;
    private static JLabel providerLabelInfo;
    private static JLabel importLabelInfo;
    private static JLabel benefitLabelInfo;
    private static JLabel paidLabelInfo;

    private static JLabel clientTextInfo;
    private static JLabel providerTextInfo;
    private static JLabel importTextInfo;
    private static JLabel benefitTextInfo;

    private static JComboBox clientChoise;
    private static JComboBox providerChoise;
    private static JComboBox paidChoise;

    private static JTextField importText;

    private void completeClientBox() {
        for (int i = 0; i < arrayClients.size(); i++) {
            clientChoise.addItem(arrayClients.get(i).getName() + " " + arrayClients.get(i).getLastname());
        }
    }

    private void completeProviderBox() {
        for (int i = 0; i < arrayProviders.size(); i++) {
            providerChoise.addItem(arrayProviders.get(i).getName());
        }
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == clientChoise) {
            String clientSelected = (String)clientChoise.getSelectedItem();
            clientTextInfo.setText(clientSelected);
        }

        if (e.getSource() == providerChoise) {
            String providerSelected = (String)providerChoise.getSelectedItem();
            providerTextInfo.setText(providerSelected);
        }
    }

    private void createTransaction() {
        con.addTransaction(clientChoise.getSelectedIndex() + 1, providerChoise.getSelectedIndex() + 1, Float.parseFloat(benefitTextInfo.getText()), paidChoise.getSelectedIndex());
    }

    protected void createMenu() {
        menubar = new JMenuBar();

        //***************************************MAIN MENU******************************************
        JMenu addMenu = new JMenu("Main Menu");
        addMenu.setMnemonic(KeyEvent.VK_F);
        addMenu.setToolTipText("Go to Main Menu");
        addMenu.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new Main().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(addMenu);

        //***************************************CLIENT MENU******************************************
        JMenu clientMenu = new JMenu("Client");
        clientMenu.setMnemonic(KeyEvent.VK_F);
        clientMenu.setToolTipText("Client");
        clientMenu.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new ClientView().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(clientMenu);

        //***************************************PROVIDER MENU******************************************
        JMenu providerMenu = new JMenu("Provider");
        providerMenu.setMnemonic(KeyEvent.VK_F);
        providerMenu.setToolTipText("Provider");
        providerMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new ProviderView().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(providerMenu);

        //***************************************EXIT MENU******************************************
        JMenu exitMenu = new JMenu("Exit");
        exitMenu.setMnemonic(KeyEvent.VK_F);
        exitMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(exitMenu);

        //***************************************SETTING UP MENU******************************************
        setJMenuBar(menubar);

        setTitle("Add Transaction");
        setSize(800, 400);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public AddTransaction() {
        arrayClients = con.getClients();
        arrayProviders = con.getProviders();

        setBounds(500, 200, 800, 400);
        contentPanel = new JPanel();
        SpringLayout layout = new SpringLayout();
        contentPanel.setLayout(layout);

        //***************************************WEST REGION******************************************
        clientLabel = new JLabel("Client:");
        providerLabel = new JLabel("Provider:");
        importLabel = new JLabel("Import:");

        clientChoise = new JComboBox();
        providerChoise = new JComboBox();
        importText = new JTextField();

        importText.setPreferredSize(new Dimension(70, 30));
        importText.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                importTextInfo.setText(importText.getText());

                if (!importTextInfo.getText().isEmpty()) {
                    int i = providerChoise.getSelectedIndex();
                    int commission = arrayProviders.get(i).getCommission();
                    int totalImport = Integer.parseInt(importTextInfo.getText());

                    float benefit = (totalImport * commission) / 100;

                    benefitTextInfo.setText(Float.toString(benefit));
                }
            }

            public void removeUpdate(DocumentEvent e) {
                importTextInfo.setText(importText.getText());

                if (!importTextInfo.getText().isEmpty()) {
                    int i = providerChoise.getSelectedIndex();
                    int commission = arrayProviders.get(i).getCommission();
                    int totalImport = Integer.parseInt(importTextInfo.getText());
                    System.out.println(i + " " + commission + " " + totalImport);
                    float benefit = (totalImport * commission) / 100;

                    benefitTextInfo.setText(Float.toString(benefit));
                }
            }

            public void insertUpdate(DocumentEvent e) {
                importTextInfo.setText(importText.getText());

                if (!importTextInfo.getText().isEmpty()) {
                    int i = providerChoise.getSelectedIndex();
                    int commission = arrayProviders.get(i).getCommission();
                    int totalImport = Integer.parseInt(importTextInfo.getText());
                    System.out.println(i + " " + commission + " " + totalImport);
                    float benefit = (totalImport * commission) / 100;

                    benefitTextInfo.setText(Float.toString(benefit));
                }
            }
        });

        clientChoise.addItemListener(this);
        providerChoise.addItemListener(this);

        contentPanel.add(clientLabel);
        contentPanel.add(providerLabel);
        contentPanel.add(importLabel);
        contentPanel.add(clientChoise);
        contentPanel.add(providerChoise);
        contentPanel.add(importText);

        layout.putConstraint(SpringLayout.WEST, clientLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, clientLabel, 50, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, clientChoise, 65, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, clientChoise, 45, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, providerLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, providerLabel, 90, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, providerChoise, 80, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, providerChoise, 85, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, importLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, importLabel, 130, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, importText, 75, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, importText, 125, SpringLayout.WEST, contentPanel);

        //***************************************EAST REGION******************************************
        infoBox = new JLabel("INFORMATION ABOUT THE NEW TRANSACTION:");
        clientLabelInfo = new JLabel("Client:");
        providerLabelInfo = new JLabel("Provider:");
        importLabelInfo = new JLabel("Total import:");
        benefitLabelInfo = new JLabel("Benefit:");
        paidLabelInfo = new JLabel("Paid:");

        clientTextInfo = new JLabel("N/A");
        providerTextInfo = new JLabel("N/A");
        importTextInfo = new JLabel("0");
        benefitTextInfo = new JLabel("0");
        paidChoise = new JComboBox();
        paidChoise.addItem("NO");
        paidChoise.addItem("YES");

        contentPanel.add(infoBox);
        contentPanel.add(clientLabelInfo);
        contentPanel.add(providerLabelInfo);
        contentPanel.add(importLabelInfo);
        contentPanel.add(benefitLabelInfo);
        contentPanel.add(paidLabelInfo);
        contentPanel.add(clientTextInfo);
        contentPanel.add(providerTextInfo);
        contentPanel.add(importTextInfo);
        contentPanel.add(benefitTextInfo);
        contentPanel.add(paidChoise);

        layout.putConstraint(SpringLayout.WEST, infoBox, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, infoBox, 20, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, clientLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, clientLabelInfo, 50, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, providerLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, providerLabelInfo, 80, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, importLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, importLabelInfo, 110, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, benefitLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, benefitLabelInfo, 140, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, paidLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, paidLabelInfo, 170, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, clientTextInfo, 460, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, clientTextInfo, 50, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, providerTextInfo, 460, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, providerTextInfo, 80, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, importTextInfo, 490, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, importTextInfo, 110, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, benefitTextInfo, 450, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, benefitTextInfo, 140, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, paidChoise, 440, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, paidChoise, 165, SpringLayout.WEST, contentPanel);

        //***************************************SOUTH REGION******************************************
        btnCancelTransaction = new JButton("Cancel Transaction");
        btnCancelTransaction.setBounds(1380, 1180, 118, 23);
        btnCancelTransaction.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                int seleccion = JOptionPane.showOptionDialog(
                        contentPanel,
                        "Do you want to cancel this transaction?",
                        "Add Transaction",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,    // null para icono por defecto.
                        new Object[]{"YES", "NO"},   // null para YES, NO y CANCEL
                        "YES");

                if (seleccion == 0) {
                    dispose();
                    new Main().setVisible(true);
                }

            }
        });

        layout.putConstraint(SpringLayout.WEST, btnCancelTransaction, 150, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, btnCancelTransaction, 300, SpringLayout.WEST, contentPanel);

        btnAddTransaction = new JButton("Add Transaction");
        btnAddTransaction.setBounds(1480, 1180, 118, 23);
        btnAddTransaction.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                int seleccion = JOptionPane.showOptionDialog(
                        contentPanel,
                        "Do you want to add this transaction?",
                        "Add Transaction",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,    // null para icono por defecto.
                        new Object[]{"Add", "Cancel"},   // null para YES, NO y CANCEL
                        "Add");

                if (seleccion == 0) {
                    createTransaction();

                    dispose();
                    new Main().setVisible(true);
                }
            }
        });

        layout.putConstraint(SpringLayout.WEST, btnAddTransaction, 450, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, btnAddTransaction, 300, SpringLayout.WEST, contentPanel);

        contentPanel.add(btnAddTransaction);
        contentPanel.add(btnCancelTransaction);
        btnCancelTransaction.setVisible(true);
        btnAddTransaction.setVisible(true);

        //***************************************END SOUTH REGION******************************************
        this.completeProviderBox();
        this.completeClientBox();
        this.createMenu();
        this.add(contentPanel);
    }
}

