/**
 * Created by AleM on 02/02/15.
 */

public class Client {
    private int id;
    private String name;
    private String lastname;
    private String dni;
    private String location;
    private String postalCode;
    private String telephone;

    public Client() {

    }

    public Client(int id, String name, String lastname, String dni, String location, String postalCode, String telephone) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.dni = dni;
        this.location = location;
        this.postalCode = postalCode;
        this.telephone = telephone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}

