import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * Created by AleM on 24/02/15.
 */
public class ModifyProvider extends JFrame {
    private static DBConnection con = new DBConnection();
    private Provider provider;

    private static JPanel contentPanel;
    private static JMenuBar menubar;

    private static JButton btnAddProvider;
    private static JButton btnCancelProvider;

    private static JLabel nameLabel;
    private static JLabel attendantLabel;
    private static JLabel dniLabel;
    private static JLabel locationLabel;
    private static JLabel postalCodeLabel;
    private static JLabel telephoneLabel;
    private static JLabel comissionLabel;

    private static JLabel infoBox;
    private static JLabel nameLabelInfo;
    private static JLabel attendantLabelInfo;
    private static JLabel dniLabelInfo;
    private static JLabel locationLabelInfo;
    private static JLabel postalCodeLabelInfo;
    private static JLabel telephoneLabelInfo;
    private static JLabel comissionLabelInfo;

    private static JLabel nameTextInfo;
    private static JLabel attendantTextInfo;
    private static JLabel dniTextInfo;
    private static JLabel locationTextInfo;
    private static JLabel postalCodeTextInfo;
    private static JLabel telephoneTextInfo;
    private static JLabel comissionTextInfo;

    private static JTextField nameText;
    private static JTextField attendantText;
    private static JTextField dniText;
    private static JTextField locationText;
    private static JTextField postalCodeText;
    private static JTextField telephoneText;
    private static JTextField comissionText;

    private void modifyProvider() {
        //This var save the int value without %
        String finalCommission =  comissionTextInfo.getText();
        finalCommission = finalCommission.substring(0, finalCommission.length() - 1);

        con.modifyProvider(provider.getId(), nameTextInfo.getText(), attendantTextInfo.getText(), dniTextInfo.getText(), locationTextInfo.getText(), postalCodeTextInfo.getText(), telephoneTextInfo.getText(), finalCommission);
    }

    protected void createMenu() {
        menubar = new JMenuBar();

        //***************************************MAIN MENU******************************************
        JMenu mainMenu = new JMenu("Main Menu");
        mainMenu.setMnemonic(KeyEvent.VK_F);
        mainMenu.setToolTipText("Go to Main Menu");
        mainMenu.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new Main().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(mainMenu);

        //***************************************ADD MENU******************************************
        JMenu addMenu = new JMenu("Add Transaction");
        addMenu.setMnemonic(KeyEvent.VK_F);
        addMenu.setToolTipText("Add Transaction");
        addMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new AddTransaction().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(addMenu);

        //***************************************CLIENT MENU******************************************
        JMenu clientMenu = new JMenu("Client");
        clientMenu.setMnemonic(KeyEvent.VK_F);
        clientMenu.setToolTipText("Client");
        clientMenu.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new ClientView().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(clientMenu);

        //***************************************PROVIDER MENU******************************************
        JMenu providerMenu = new JMenu("Provider");
        providerMenu.setMnemonic(KeyEvent.VK_F);
        providerMenu.setToolTipText("Provider");
        providerMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new ProviderView().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(providerMenu);

        //***************************************EXIT MENU******************************************
        JMenu exitMenu = new JMenu("Exit");
        exitMenu.setMnemonic(KeyEvent.VK_F);
        exitMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(exitMenu);

        //***************************************SETTING UP MENU******************************************
        setJMenuBar(menubar);

        setTitle("Modify Provider");
        setSize(800, 400);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public ModifyProvider(final Provider provider) {
        this.provider = provider;

        setBounds(500, 200, 800, 400);
        contentPanel = new JPanel();
        SpringLayout layout = new SpringLayout();
        contentPanel.setLayout(layout);

        //***************************************WEST REGION******************************************
        nameLabel = new JLabel("Name:");
        attendantLabel = new JLabel("Attendant:");
        dniLabel = new JLabel("DNI:");
        locationLabel = new JLabel("Location:");
        postalCodeLabel = new JLabel("Postal Code:");
        telephoneLabel = new JLabel("Telephone:");
        comissionLabel = new JLabel("Commission:");

        nameText = new JTextField(provider.getName());

        nameText.setPreferredSize(new Dimension(100, 30));
        nameText.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                nameTextInfo.setText(nameText.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                nameTextInfo.setText(nameText.getText());
            }

            public void insertUpdate(DocumentEvent e) {
                nameTextInfo.setText(nameText.getText());
            }
        });

        attendantText = new JTextField(provider.getAttendant());

        attendantText.setPreferredSize(new Dimension(100, 30));
        attendantText.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                attendantTextInfo.setText(attendantText.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                attendantTextInfo.setText(attendantText.getText());
            }

            public void insertUpdate(DocumentEvent e) {
                attendantTextInfo.setText(attendantText.getText());
            }
        });

        dniText = new JTextField(provider.getDni());

        dniText.setPreferredSize(new Dimension(100, 30));
        dniText.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                dniTextInfo.setText(dniText.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                dniTextInfo.setText(dniText.getText());
            }

            public void insertUpdate(DocumentEvent e) {
                dniTextInfo.setText(dniText.getText());
            }
        });

        locationText = new JTextField(provider.getLocation());

        locationText.setPreferredSize(new Dimension(100, 30));
        locationText.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                locationTextInfo.setText(locationText.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                locationTextInfo.setText(locationText.getText());
            }

            public void insertUpdate(DocumentEvent e) {
                locationTextInfo.setText(locationText.getText());
            }
        });

        postalCodeText = new JTextField(provider.getPostalCode());

        postalCodeText.setPreferredSize(new Dimension(100, 30));
        postalCodeText.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                postalCodeTextInfo.setText(postalCodeText.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                postalCodeTextInfo.setText(postalCodeText.getText());
            }

            public void insertUpdate(DocumentEvent e) {
                postalCodeTextInfo.setText(postalCodeText.getText());
            }
        });

        telephoneText = new JTextField(provider.getTelephone());

        telephoneText.setPreferredSize(new Dimension(100, 30));
        telephoneText.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                telephoneTextInfo.setText(telephoneText.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                telephoneTextInfo.setText(telephoneText.getText());
            }

            public void insertUpdate(DocumentEvent e) {
                telephoneTextInfo.setText(telephoneText.getText());
            }
        });

        comissionText = new JTextField(Integer.toString(provider.getCommission()));

        comissionText.setPreferredSize(new Dimension(100, 30));
        comissionText.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                comissionTextInfo.setText(comissionText.getText() + "%");
            }

            public void removeUpdate(DocumentEvent e) {
                comissionTextInfo.setText(comissionText.getText() + "%");
            }

            public void insertUpdate(DocumentEvent e) {
                comissionTextInfo.setText(comissionText.getText() + "%");
            }
        });

        contentPanel.add(nameLabel);
        contentPanel.add(attendantLabel);
        contentPanel.add(dniLabel);
        contentPanel.add(locationLabel);
        contentPanel.add(postalCodeLabel);
        contentPanel.add(telephoneLabel);
        contentPanel.add(comissionLabel);
        contentPanel.add(nameText);
        contentPanel.add(attendantText);
        contentPanel.add(dniText);
        contentPanel.add(locationText);
        contentPanel.add(postalCodeText);
        contentPanel.add(telephoneText);
        contentPanel.add(comissionText);

        layout.putConstraint(SpringLayout.WEST, nameLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, nameLabel, 50, SpringLayout.WEST, contentPanel);;

        layout.putConstraint(SpringLayout.WEST, attendantLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, attendantLabel, 80, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, dniLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, dniLabel, 110, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, locationLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, locationLabel, 140, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, postalCodeLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, postalCodeLabel, 170, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, telephoneLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, telephoneLabel, 200, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, comissionLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, comissionLabel, 230, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, nameText, 65, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, nameText, 45, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, attendantText, 95, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, attendantText, 75, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, dniText, 55, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, dniText, 105, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, locationText, 85, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, locationText, 135, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, postalCodeText, 100, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, postalCodeText, 165, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, telephoneText, 100, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, telephoneText, 195, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, comissionText, 100, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, comissionText, 225, SpringLayout.WEST, contentPanel);

        //***************************************EAST REGION******************************************
        infoBox = new JLabel("INFORMATION ABOUT THE NEW PROVIDER:");
        nameLabelInfo = new JLabel("Name:");
        attendantLabelInfo = new JLabel("Attendant:");
        dniLabelInfo = new JLabel("DNI:");
        locationLabelInfo = new JLabel("Location:");
        postalCodeLabelInfo = new JLabel("Postal Code:");
        telephoneLabelInfo = new JLabel("Telephone:");
        comissionLabelInfo = new JLabel("Commission:");

        nameTextInfo = new JLabel(provider.getName());
        attendantTextInfo = new JLabel(provider.getAttendant());
        dniTextInfo = new JLabel(provider.getDni());
        locationTextInfo = new JLabel(provider.getLocation());
        postalCodeTextInfo = new JLabel(provider.getPostalCode());
        telephoneTextInfo = new JLabel(provider.getTelephone());
        comissionTextInfo = new JLabel(Integer.toString(provider.getCommission()) + "%");

        contentPanel.add(infoBox);
        contentPanel.add(nameLabelInfo);
        contentPanel.add(attendantLabelInfo);
        contentPanel.add(dniLabelInfo);
        contentPanel.add(locationLabelInfo);
        contentPanel.add(postalCodeLabelInfo);
        contentPanel.add(telephoneLabelInfo);
        contentPanel.add(comissionLabelInfo);
        contentPanel.add(nameTextInfo);
        contentPanel.add(attendantTextInfo);
        contentPanel.add(dniTextInfo);
        contentPanel.add(locationTextInfo);
        contentPanel.add(postalCodeTextInfo);
        contentPanel.add(telephoneTextInfo);
        contentPanel.add(comissionTextInfo);

        layout.putConstraint(SpringLayout.WEST, infoBox, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, infoBox, 20, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, nameLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, nameLabelInfo, 50, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, attendantLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, attendantLabelInfo, 80, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, dniLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, dniLabelInfo, 110, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, locationLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, locationLabelInfo, 140, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, postalCodeLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, postalCodeLabelInfo, 170, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, telephoneLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, telephoneLabelInfo, 200, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, comissionLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, comissionLabelInfo, 230, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, nameTextInfo, 445, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, nameTextInfo, 50, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, attendantTextInfo, 475, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, attendantTextInfo, 80, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, dniTextInfo, 430, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, dniTextInfo, 110, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, locationTextInfo, 460, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, locationTextInfo, 140, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, postalCodeTextInfo, 480, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, postalCodeTextInfo, 170, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, telephoneTextInfo, 475, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, telephoneTextInfo, 200, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, comissionTextInfo, 490, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, comissionTextInfo, 230, SpringLayout.WEST, contentPanel);

        //***************************************SOUTH REGION******************************************
        btnCancelProvider = new JButton("Delete Provider");
        btnCancelProvider.setBounds(1380, 1180, 118, 23);
        btnCancelProvider.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                int seleccion = JOptionPane.showOptionDialog(
                        contentPanel,
                        "Do you want to delete this provider?",
                        "Modify Provider",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,    // null para icono por defecto.
                        new Object[]{"YES", "NO"},   // null para YES, NO y CANCEL
                        "YES");

                if (seleccion == 0) {
                    ArrayList<Benefit> providerTransactions = new ArrayList<Benefit>();
                    providerTransactions = con.getUserTransactions(provider.getId());

                    if (providerTransactions.size() > 0) {
                        JOptionPane.showMessageDialog(null, "First,you have to delete all the transactions of this provider!");
                    }

                    else {
                        con.deleteProvider(provider.getId());
                    }

                    dispose();
                    new ProviderView().setVisible(true);
                }

            }
        });

        layout.putConstraint(SpringLayout.WEST, btnCancelProvider, 150, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, btnCancelProvider, 300, SpringLayout.WEST, contentPanel);

        btnAddProvider = new JButton("Modify Provider");
        btnAddProvider.setBounds(1480, 1180, 118, 23);
        btnAddProvider.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                int seleccion = JOptionPane.showOptionDialog(
                        contentPanel,
                        "Do you want to modify this provider?",
                        "Modify Provider",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,    // null para icono por defecto.
                        new Object[]{"Modify", "Cancel"},   // null para YES, NO y CANCEL
                        "Modify");

                if (seleccion == 0) {
                    modifyProvider();

                    dispose();
                    new ProviderView().setVisible(true);
                }
            }
        });

        layout.putConstraint(SpringLayout.WEST, btnAddProvider, 450, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, btnAddProvider, 300, SpringLayout.WEST, contentPanel);

        contentPanel.add(btnAddProvider);
        contentPanel.add(btnCancelProvider);
        btnCancelProvider.setVisible(true);
        btnAddProvider.setVisible(true);

        //***************************************END SOUTH REGION******************************************
        this.createMenu();
        this.add(contentPanel);
    }
}
