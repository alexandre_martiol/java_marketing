import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * Created by AleM on 13/02/15.
 */
public class ProviderView extends JFrame {
    private static DBConnection con = new DBConnection();
    private static JPanel contentPanel;

    private static JMenuBar menubar;

    private JTable providersTable;
    private DefaultTableModel model;

    private JButton btnModifyProvider;
    private JButton btnAddProvider;

    private ArrayList<Provider> providersArray;

    private TableRowSorter<TableModel> rowSorter;

    private JTextField textFieldFilter;

    private JLabel searchLabel;
    //*************************************** END DEFINITION OF VARIABLES ******************************************

    private void resetProvidersTable() {
        model.setRowCount(0);
    }

    private void addInfoInTable() {
        //instruccio SQL PER BUSCAR TOTS ELS REGISTRES DE LA TAULA BENEFIT
        providersArray = con.getProviders();

        for (int i = 0; i < providersArray.size(); i++) {
            //Creamos un Objeto con tantos parámetros como datos retorne cada fila de la consulta
            Object[] fila = new Object[7];

            //Lo que hay entre comillas son los campos de la base de datos
            //fila[0] = clientsArray.get(i).getId();
            fila[0] = providersArray.get(i).getName();
            fila[1] = providersArray.get(i).getAttendant();
            fila[2] = providersArray.get(i).getLocation();
            fila[3] = providersArray.get(i).getPostalCode();
            fila[4] = providersArray.get(i).getDni();
            fila[5] = providersArray.get(i).getTelephone();
            fila[6] = providersArray.get(i).getCommission();

            model.addRow(fila); // Añade una fila al final del model de la tabla
        }
    }

    protected void createMenu() {
        menubar = new JMenuBar();

        //***************************************MAIN MENU******************************************
        JMenu mainMenu = new JMenu("Main Menu");
        mainMenu.setMnemonic(KeyEvent.VK_F);
        mainMenu.setToolTipText("Go to Main Menu");
        mainMenu.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new Main().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(mainMenu);

        //***************************************ADD MENU******************************************
        JMenu addMenu = new JMenu("Add Transaction");
        addMenu.setMnemonic(KeyEvent.VK_F);
        addMenu.setToolTipText("Add new transaction");
        addMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new AddTransaction().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(addMenu);

        //***************************************CLIENT MENU******************************************
        JMenu clientMenu = new JMenu("Client");
        clientMenu.setMnemonic(KeyEvent.VK_F);
        clientMenu.setToolTipText("Client");
        clientMenu.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new ClientView().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(clientMenu);

        //***************************************EXIT MENU******************************************
        JMenu exitMenu = new JMenu("Exit");
        exitMenu.setMnemonic(KeyEvent.VK_F);
        exitMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(exitMenu);

        //***************************************SETTING UP MENU******************************************
        setJMenuBar(menubar);

        setTitle("Provider Management");
        setSize(500, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public ProviderView() {
        // boto nova venta configuracio , preparacio event I crida a seguent pantalla de la classe AltesFrame
        setBounds(500, 200, 500, 600);
        contentPanel = new JPanel();
        SpringLayout layout = new SpringLayout();
        contentPanel.setLayout(layout);

        //***************************************WEST REGION******************************************
        JScrollPane scrollPanel = new JScrollPane();
        scrollPanel.setBounds(20, 50, 300, 300);

        layout.putConstraint(SpringLayout.WEST, scrollPanel, 25, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, scrollPanel, 50, SpringLayout.WEST, contentPanel);

        contentPanel.add(scrollPanel);

        try {
            //PREPARACIO DE LA TAULA , Afegim columnes a la taula (les que volguem)
            model = new DefaultTableModel() {
                @Override
                public boolean isCellEditable(int row, int column) {
                    //all cells false
                    return false;
                }
            };

            //model.addColumn("Num");
            model.addColumn("Name");
            model.addColumn("Attendant");
            model.addColumn("Location");
            model.addColumn("PostalCode");
            model.addColumn("DNI");
            model.addColumn("Telephone");
            model.addColumn("Commission");

            addInfoInTable();

            //Esto añade la tabla al portView del scrollPanel
            providersTable = new JTable(model);
            TableColumnModel columnModel = providersTable.getColumnModel();
            columnModel.getColumn(0).setPreferredWidth(80);
            columnModel.getColumn(1).setPreferredWidth(20);
            columnModel.getColumn(2).setPreferredWidth(20);
            columnModel.getColumn(3).setPreferredWidth(20);
            columnModel.getColumn(4).setPreferredWidth(20);
            columnModel.getColumn(5).setPreferredWidth(50);
            columnModel.getColumn(6).setPreferredWidth(10);

            scrollPanel.setViewportView(providersTable);

        } catch (Exception e) {
            e.printStackTrace();
        }

        //***************************************SOUTH REGION******************************************
        btnModifyProvider = new JButton("Modify Provider");
        btnModifyProvider.setBounds(1480, 1180, 118, 23);
        btnModifyProvider.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                int selectedRow = providersTable.getSelectedRow();

                //If not selected row...
                if (selectedRow == -1) {
                    JOptionPane.showMessageDialog(null, "Please first select a client.");
                }

                else {
                    int selectedProvider = providersArray.get(selectedRow).getId();

                    Provider provider = con.getProvider(selectedProvider);

                    dispose();
                    new ModifyProvider(provider).setVisible(true);
                }
            }
        });

        layout.putConstraint(SpringLayout.WEST, btnModifyProvider, 60, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, btnModifyProvider, 500, SpringLayout.WEST, contentPanel);
        contentPanel.add(btnModifyProvider);

        btnAddProvider = new JButton("Add new Provider");
        btnAddProvider.setBounds(1480, 1180, 118, 23);
        btnAddProvider.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                dispose();
                new AddProvider().setVisible(true);
            }
        });

        layout.putConstraint(SpringLayout.WEST, btnAddProvider, 300, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, btnAddProvider, 500, SpringLayout.WEST, contentPanel);
        contentPanel.add(btnAddProvider);

        //***************************************NORTH REGION******************************************
        this.createMenu();

        searchLabel = new JLabel("Search:");
        layout.putConstraint(SpringLayout.WEST, searchLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, searchLabel, 20, SpringLayout.WEST, contentPanel);

        contentPanel.add(searchLabel);

        rowSorter = new TableRowSorter<TableModel>(providersTable.getModel());
        textFieldFilter = new JTextField();
        textFieldFilter.setPreferredSize(new Dimension(150, 30));

        providersTable.setRowSorter(rowSorter);

        layout.putConstraint(SpringLayout.WEST, textFieldFilter, 70, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, textFieldFilter, 15, SpringLayout.WEST, contentPanel);
        contentPanel.add(textFieldFilter);

        textFieldFilter.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                String text = textFieldFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                String text = textFieldFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });

        //*************************************** CONFIGURATION ******************************************
        btnModifyProvider.setVisible(true);
        btnAddProvider.setVisible(true);
        contentPanel.setVisible(true);
        this.add(contentPanel);
    }
}
