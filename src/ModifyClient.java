/**
 * Created by AleM on 24/04/15.
 */
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public class ModifyClient extends JFrame {
    private static DBConnection con = new DBConnection();
    private Client client;

    private static JPanel contentPanel;
    private static JMenuBar menubar;

    private static JButton btnAddClient;
    private static JButton btnCancelClient;

    private static JLabel nameLabel;
    private static JLabel lastNameLabel;
    private static JLabel dniLabel;
    private static JLabel locationLabel;
    private static JLabel postalCodeLabel;
    private static JLabel telephoneLabel;

    private static JLabel infoBox;
    private static JLabel nameLabelInfo;
    private static JLabel lastNameLabelInfo;
    private static JLabel dniLabelInfo;
    private static JLabel locationLabelInfo;
    private static JLabel postalCodeLabelInfo;
    private static JLabel telephoneLabelInfo;

    private static JLabel nameTextInfo;
    private static JLabel lastNameTextInfo;
    private static JLabel dniTextInfo;
    private static JLabel locationTextInfo;
    private static JLabel postalCodeTextInfo;
    private static JLabel telephoneTextInfo;

    private static JTextField nameText;
    private static JTextField lastNameText;
    private static JTextField dniText;
    private static JTextField locationText;
    private static JTextField postalCodeText;
    private static JTextField telephoneText;

    private void modifyClient() {
        con.modifyClient(client.getId(), nameTextInfo.getText(), lastNameTextInfo.getText(), dniTextInfo.getText(), locationTextInfo.getText(), postalCodeTextInfo.getText(), telephoneTextInfo.getText());
    }

    protected void createMenu() {
        menubar = new JMenuBar();

        //***************************************MAIN MENU******************************************
        JMenu mainMenu = new JMenu("Main Menu");
        mainMenu.setMnemonic(KeyEvent.VK_F);
        mainMenu.setToolTipText("Go to Main Menu");
        mainMenu.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new Main().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(mainMenu);

        //***************************************ADD MENU******************************************
        JMenu addMenu = new JMenu("Add Transaction");
        addMenu.setMnemonic(KeyEvent.VK_F);
        addMenu.setToolTipText("Add Transaction");
        addMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new AddTransaction().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(addMenu);

        //***************************************CLIENT MENU******************************************
        JMenu clientMenu = new JMenu("Client");
        clientMenu.setMnemonic(KeyEvent.VK_F);
        clientMenu.setToolTipText("Client");
        clientMenu.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new ClientView().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(clientMenu);

        //***************************************PROVIDER MENU******************************************
        JMenu providerMenu = new JMenu("Provider");
        providerMenu.setMnemonic(KeyEvent.VK_F);
        providerMenu.setToolTipText("Provider");
        providerMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                new ProviderView().setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(providerMenu);

        //***************************************EXIT MENU******************************************
        JMenu exitMenu = new JMenu("Exit");
        exitMenu.setMnemonic(KeyEvent.VK_F);
        exitMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        menubar.add(exitMenu);

        //***************************************SETTING UP MENU******************************************
        setJMenuBar(menubar);

        setTitle("Modify Client");
        setSize(800, 400);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public ModifyClient(final Client client) {
        this.client = client;

        setBounds(500, 200, 800, 400);
        contentPanel = new JPanel();
        SpringLayout layout = new SpringLayout();
        contentPanel.setLayout(layout);

        //***************************************WEST REGION******************************************
        nameLabel = new JLabel("Name:");
        lastNameLabel = new JLabel("Last Name:");
        dniLabel = new JLabel("DNI:");
        locationLabel = new JLabel("Location:");
        postalCodeLabel = new JLabel("Postal Code:");
        telephoneLabel = new JLabel("Telephone:");

        nameText = new JTextField(client.getName());

        nameText.setPreferredSize(new Dimension(100, 30));
        nameText.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                nameTextInfo.setText(nameText.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                nameTextInfo.setText(nameText.getText());
            }

            public void insertUpdate(DocumentEvent e) {
                nameTextInfo.setText(nameText.getText());
            }
        });

        lastNameText = new JTextField(client.getLastname());

        lastNameText.setPreferredSize(new Dimension(100, 30));
        lastNameText.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                lastNameTextInfo.setText(lastNameText.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                lastNameTextInfo.setText(lastNameText.getText());
            }

            public void insertUpdate(DocumentEvent e) {
                lastNameTextInfo.setText(lastNameText.getText());
            }
        });

        dniText = new JTextField(client.getDni());

        dniText.setPreferredSize(new Dimension(100, 30));
        dniText.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                dniTextInfo.setText(dniText.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                dniTextInfo.setText(dniText.getText());
            }

            public void insertUpdate(DocumentEvent e) {
                dniTextInfo.setText(dniText.getText());
            }
        });

        locationText = new JTextField(client.getLocation());

        locationText.setPreferredSize(new Dimension(100, 30));
        locationText.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                locationTextInfo.setText(locationText.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                locationTextInfo.setText(locationText.getText());
            }

            public void insertUpdate(DocumentEvent e) {
                locationTextInfo.setText(locationText.getText());
            }
        });

        postalCodeText = new JTextField(client.getPostalCode());

        postalCodeText.setPreferredSize(new Dimension(100, 30));
        postalCodeText.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                postalCodeTextInfo.setText(postalCodeText.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                postalCodeTextInfo.setText(postalCodeText.getText());
            }

            public void insertUpdate(DocumentEvent e) {
                postalCodeTextInfo.setText(postalCodeText.getText());
            }
        });

        telephoneText = new JTextField(client.getTelephone());

        telephoneText.setPreferredSize(new Dimension(100, 30));
        telephoneText.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                telephoneTextInfo.setText(telephoneText.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                telephoneTextInfo.setText(telephoneText.getText());
            }

            public void insertUpdate(DocumentEvent e) {
                telephoneTextInfo.setText(telephoneText.getText());
            }
        });

        contentPanel.add(nameLabel);
        contentPanel.add(lastNameLabel);
        contentPanel.add(dniLabel);
        contentPanel.add(locationLabel);
        contentPanel.add(postalCodeLabel);
        contentPanel.add(telephoneLabel);
        contentPanel.add(nameText);
        contentPanel.add(lastNameText);
        contentPanel.add(dniText);
        contentPanel.add(locationText);
        contentPanel.add(postalCodeText);
        contentPanel.add(telephoneText);

        layout.putConstraint(SpringLayout.WEST, nameLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, nameLabel, 50, SpringLayout.WEST, contentPanel);;

        layout.putConstraint(SpringLayout.WEST, lastNameLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, lastNameLabel, 80, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, dniLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, dniLabel, 110, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, locationLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, locationLabel, 140, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, postalCodeLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, postalCodeLabel, 170, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, telephoneLabel, 20, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, telephoneLabel, 200, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, nameText, 65, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, nameText, 45, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, lastNameText, 95, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, lastNameText, 75, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, dniText, 55, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, dniText, 105, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, locationText, 85, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, locationText, 135, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, postalCodeText, 100, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, postalCodeText, 165, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, telephoneText, 100, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, telephoneText, 195, SpringLayout.WEST, contentPanel);

        //***************************************EAST REGION******************************************
        infoBox = new JLabel("INFORMATION ABOUT THE CLIENT:");
        nameLabelInfo = new JLabel("Name:");
        lastNameLabelInfo = new JLabel("Last Name:");
        dniLabelInfo = new JLabel("DNI:");
        locationLabelInfo = new JLabel("Location:");
        postalCodeLabelInfo = new JLabel("Postal Code:");
        telephoneLabelInfo = new JLabel("Telephone:");

        nameTextInfo = new JLabel(client.getName());
        lastNameTextInfo = new JLabel(client.getLastname());
        dniTextInfo = new JLabel(client.getDni());
        locationTextInfo = new JLabel(client.getLocation());
        postalCodeTextInfo = new JLabel(client.getPostalCode());
        telephoneTextInfo = new JLabel(client.getTelephone());

        contentPanel.add(infoBox);
        contentPanel.add(nameLabelInfo);
        contentPanel.add(lastNameLabelInfo);
        contentPanel.add(dniLabelInfo);
        contentPanel.add(locationLabelInfo);
        contentPanel.add(postalCodeLabelInfo);
        contentPanel.add(telephoneLabelInfo);
        contentPanel.add(nameTextInfo);
        contentPanel.add(lastNameTextInfo);
        contentPanel.add(dniTextInfo);
        contentPanel.add(locationTextInfo);
        contentPanel.add(postalCodeTextInfo);
        contentPanel.add(telephoneTextInfo);

        layout.putConstraint(SpringLayout.WEST, infoBox, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, infoBox, 20, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, nameLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, nameLabelInfo, 50, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, lastNameLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, lastNameLabelInfo, 80, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, dniLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, dniLabelInfo, 110, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, locationLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, locationLabelInfo, 140, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, postalCodeLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, postalCodeLabelInfo, 170, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, telephoneLabelInfo, 400, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, telephoneLabelInfo, 200, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, nameTextInfo, 445, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, nameTextInfo, 50, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, lastNameTextInfo, 475, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, lastNameTextInfo, 80, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, dniTextInfo, 430, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, dniTextInfo, 110, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, locationTextInfo, 460, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, locationTextInfo, 140, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, postalCodeTextInfo, 480, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, postalCodeTextInfo, 170, SpringLayout.WEST, contentPanel);

        layout.putConstraint(SpringLayout.WEST, telephoneTextInfo, 475, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, telephoneTextInfo, 200, SpringLayout.WEST, contentPanel);

        //***************************************SOUTH REGION******************************************
        btnCancelClient = new JButton("Delete Client");
        btnCancelClient.setBounds(1380, 1180, 118, 23);
        btnCancelClient.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                int seleccion = JOptionPane.showOptionDialog(
                        contentPanel,
                        "Do you want to delete this client?",
                        "Modify Client",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,    // null para icono por defecto.
                        new Object[]{"YES", "NO"},   // null para YES, NO y CANCEL
                        "YES");

                if (seleccion == 0) {
                    ArrayList<Benefit> userTransactions = new ArrayList<Benefit>();
                    userTransactions = con.getUserTransactions(client.getId());

                    if (userTransactions.size() > 0) {
                        JOptionPane.showMessageDialog(null, "First,you have to delete all the transactions of this client!");
                    }

                    else {
                        con.deleteClient(client.getId());
                    }

                    dispose();
                    new ClientView().setVisible(true);
                }

            }
        });

        layout.putConstraint(SpringLayout.WEST, btnCancelClient, 150, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, btnCancelClient, 300, SpringLayout.WEST, contentPanel);

        btnAddClient = new JButton("Modify Client");
        btnAddClient.setBounds(1480, 1180, 118, 23);
        btnAddClient.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                int seleccion = JOptionPane.showOptionDialog(
                        contentPanel,
                        "Do you want to modify this client?",
                        "Modify Client",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,    // null para icono por defecto.
                        new Object[]{"Modify", "Cancel"},   // null para YES, NO y CANCEL
                        "Modify");

                if (seleccion == 0) {
                    modifyClient();

                    dispose();
                    new ClientView().setVisible(true);
                }
            }
        });

        layout.putConstraint(SpringLayout.WEST, btnAddClient, 450, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.NORTH, btnAddClient, 300, SpringLayout.WEST, contentPanel);

        contentPanel.add(btnAddClient);
        contentPanel.add(btnCancelClient);
        btnCancelClient.setVisible(true);
        btnAddClient.setVisible(true);

        //***************************************END SOUTH REGION******************************************
        this.createMenu();
        this.add(contentPanel);
    }
}
