/**
 * Created by AleM on 02/02/15.
 */

public class Provider {
    private int id;
    private String name;
    private String attendant;
    private String location;
    private String postalCode;
    private String telephone;
    private String dni;
    private int commission;

    public Provider() {

    }

    public Provider(int id, String name, String attendant, String location, String postalCode, String telephone, String dni, int commission) {
        this.id = id;
        this.name = name;
        this.attendant = attendant;
        this.location = location;
        this.postalCode = postalCode;
        this.telephone = telephone;
        this.dni = dni;
        this.commission = commission;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttendant() {
        return attendant;
    }

    public void setAttendant(String attendant) {
        this.attendant = attendant;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }
}

