/**
 * Created by AleM on 02/02/15.
 */

public class Benefit {
    private int id;
    private int idClient;
    private int idProvider;
    private float benefit;
    private boolean paid;

    public Benefit() {
        this.id = 0;
        this.idClient = 0;
        this.idProvider = 0;
        this.benefit = 0;
        this.paid = false;
    }

    public Benefit(int id, int idClient, int idProvider, float benefit, boolean paid) {
        this.id = id;
        this.idClient = idClient;
        this.idProvider = idProvider;
        this.benefit = benefit;
        this.paid = paid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(int idProvider) {
        this.idProvider = idProvider;
    }

    public float getBenefit() {
        return benefit;
    }

    public void setBenefit(int benefit) {
        this.benefit = benefit;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }
}

